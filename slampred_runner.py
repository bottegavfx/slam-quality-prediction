"""Usage: slampred_runner.py <stage> [<args>...]

Stages:
    predict     Create prediction from a video in video-index, and split a/v
    segment     Create segments from prediction into segment-index
"""

from os import getenv
from docopt import docopt

class RunnerInterrupted(Exception):
    __module__ = Exception.__module__

if __name__ == '__main__':
    args = docopt(__doc__, version=getenv('RUNNER_VERSION', 'unknown'), options_first=True)
    argv = [args['<stage>']] + args['<args>']

    try:

        if args['<stage>'] == 'predict':
            import predict_from_video
            args = docopt(predict_from_video.__doc__.replace('predict_from_video.py', 'slampred_runner.py predict'), argv=argv)
            predict_from_video.parse_run(args)
        elif args['<stage>'] == 'segment':
            import segment_into_dl
            args = docopt(segment_into_dl.__doc__.replace('segment_into_dl.py', 'slampred_runner.py segment'), argv=argv)
            segment_into_dl.parse_run(args)

    except Exception as e:
        raise RunnerInterrupted(type(e).__name__ + ': ' + str(e)) from e