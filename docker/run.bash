#!/bin/bash

AWSCONFIG="$HOME"/.aws
WORKDIR=/tmp/media

NAME="036256485575.dkr.ecr.eu-west-1.amazonaws.com/video-data-extraction/slam-preprocessing"
LATEST="${NAME}:latest"

mkdir -p $WORKDIR

CMDLINE="docker run --ipc=host --mount source=$AWSCONFIG,target=/home/debuguser/.aws,type=bind,readonly --mount source=$WORKDIR,target=$WORKDIR,type=bind -u 1000 -it \"$LATEST\" "$@""
echo "$CMDLINE"
eval "$CMDLINE"