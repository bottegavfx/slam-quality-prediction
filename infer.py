import time
import json
import pickle
import argparse
from pathlib import Path

import numpy as np
from natsort import natsorted

from sklearn.ensemble import AdaBoostClassifier
from generate_features import generate_features, KeyPointMethod

parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('--input', help='Path to input directory with images', type=str, required=True)
parser.add_argument('--classifier', help='Input classifier filename/path', type=str, default='classifier.bin')
parser.add_argument('--output', help='Path to output prediction file', type=str, default='output.json')
parser.add_argument('--kp_method', type=lambda k: KeyPointMethod[k], choices=list(KeyPointMethod), default=KeyPointMethod.SILX_SIFT)
args = vars(parser.parse_args())

classifier_path = Path(args['classifier'])
input_path = Path(args['input'])
assert(classifier_path.exists())
assert(input_path.exists())

if input_path.is_dir():
    imgs_paths = natsorted(list(input_path.absolute().glob('*.jpg')))
    start_time = time.time()
    features_named = generate_features(imgs_paths, args['kp_method'])
    total_time = time.time() - start_time
    fps = len(imgs_paths) / total_time
    print("Features detected in {} seconds, fps: {}".format(total_time, fps))
elif input_path.is_file() and input_path.suffix == '.json':
    with open(input_path) as json_file:
        features_named = json.load(json_file)
else:
    raise RuntimeError()

assert(len(features_named) > 0)
features = [feature for name, feature in features_named.items() if name not in ['t']]
it = iter(features)
the_len = len(next(it))
assert(all(len(l) == the_len for l in it))
features = np.array([feature_seq for feature_seq in zip(*(features))])

bdt = pickle.load(open(classifier_path, 'rb'))
# pred = bdt.predict(features)
pred = list(zip(*bdt.predict_proba(features)))[1]

with open(args['output'], 'w') as outfile:
    json.dump(pred, outfile, indent=2)