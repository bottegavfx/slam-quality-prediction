import json
import pickle
import argparse
from pathlib import Path

import numpy as np

from sklearn.metrics import classification_report
from sklearn.ensemble import AdaBoostClassifier
from sklearn.tree import DecisionTreeClassifier

def load_gt(path: Path):
    gt = {}
    for f in sorted(list(path.glob('*'))):
        if f.is_dir():
            asset_id = f.name
            with open(f / 'track.json') as json_file:
                track_labels = json.load(json_file)
                gt[asset_id] = track_labels
    return gt


def dataset_to_np(fe: dict, gt: dict, asset_id: str = None):
    X_list = []
    Y_list = []
    for asset_name in fe:
        if (asset_id is None) or (asset_name is not None and asset_name == asset_id):
            features = [feature_seq for feature_seq in zip(*(fe[asset_name]))]
            ground_truth = [label for _, label in gt[asset_name].items()]
            assert(len(features) == len(ground_truth))
            Y_list += ground_truth
            X_list += features
    return np.array(X_list), np.array(Y_list)


def load_features(path: Path):
    features = {}
    for f in sorted(list(path.glob('*.json'))):
        with open(f) as json_file:
            asset_id = f.name.split('_')[0]
            features_named = json.load(json_file)
            features[asset_id] = [feature for name, feature in features_named.items() if name not in ['t']]
            it = iter(features[asset_id])
            the_len = len(next(it))
            assert(all(len(l) == the_len for l in it))
    return features

def check_gt_vs_features(gt, fe):
    assert(gt.keys() == fe.keys())
    assert(all(len(f) == len(gt[l]) for ff, l in zip(gt, fe) for f in fe[ff]))


def load_features_common(path_fe: Path, path_gt: Path):
    features = load_features(path_fe)
    assets_gt = set([p.name for p in sorted(list(path_gt.glob('*')))])
    assets_fe = set(features.keys())
    for key in assets_fe - assets_gt:
        features.pop(key)
    return features


def load_dataset(X_path: Path, Y_path: Path, asset_id: str = None):
    gt = load_gt(Y_path)
    fe = load_features_common(X_path, Y_path)
    check_gt_vs_features(gt, fe)
    X, Y = dataset_to_np(fe, gt, asset_id)
    return X, Y


parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('--train', help='Path to directory with training asset sub-directories', type=str, required=True)
parser.add_argument('--test', help='Path to directory with testing asset sub-directories', type=str, required=True)
parser.add_argument('--features', help='Path to directory with feature `*.json` files for both train and test sets', type=str, required=True)
parser.add_argument('--output', help='Output classifier filename/path', type=str, default='classifier.bin')
parser.add_argument('--plot', help='Plot testing distribution', action='store_true', default=False)
parser.add_argument('--random_state', help='Random state int to be used during DT training', type=int)
args = vars(parser.parse_args())


Y_train_path = Path(args['train'])
Y_test_path = Path(args['test'])
X_common_path = Path(args['features'])
assert(Y_train_path.exists())
assert(Y_test_path.exists())
assert(X_common_path.exists())

X_train, Y_train = load_dataset(X_common_path, Y_train_path)
X_test, Y_test = load_dataset(X_common_path, Y_test_path)

bdt = AdaBoostClassifier(base_estimator=DecisionTreeClassifier(max_depth=6, random_state=args['random_state']),
                         algorithm="SAMME.R",
                         n_estimators=100,
                         random_state=args['random_state'])

bdt.fit(X_train, Y_train)

predicted = bdt.predict(X_test)
report = classification_report(Y_test, predicted, target_names=['Failire to track', 'Succesful track'])
print(report)

if args['plot']:
    import matplotlib.pyplot as plt
    plot_colors = "br"
    class_names = "FT"
    # twoclass_output = bdt.decision_function(X_test)
    twoclass_output = np.array(list(zip(*bdt.predict_proba(X_test)))[1])
    plot_range = (twoclass_output.min(), twoclass_output.max())
    for i, n, c in zip(range(2), class_names, plot_colors):
        plt.hist(twoclass_output[Y_test == i],
                bins=50,
                range=plot_range,
                facecolor=c,
                label='Class %s' % n,
                alpha=.5,
                edgecolor='k')
    x1, x2, y1, y2 = plt.axis()
    plt.legend(loc='upper right')
    plt.show()

pickle.dump(bdt, open(args['output'], 'wb'))