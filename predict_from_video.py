"""
Usage:
    predict_from_video.py --video_id <video_id> [options]

Options:
    --video_id <video_id>          Video ID (from video-index)
    --model_id <model_id>          Model ID [default: eea65e92-9863-4589-bbac-c7a0f5e88e17]
    --output_dir <path>            Output directory [default: /tmp/datalake-slam-pred/]
    --no_upload                    Skip uploading anything to Datalake
"""

import os
import re
import time
import json
import shutil
from pathlib import Path
from typing import List

import numpy as np
from docopt import docopt

from generate_features import generate_features, KeyPointMethod

import avpp_kit.agent as avpp_agent
from avpp_kit.agent import MediaType
from avpp_kit.checkpoints.pickle import PickleCheckpoint
from avpp_kit.processing.meta.rgb import get_framelist


def split_scenes(scene_cuts: List[int], images_dir: Path, splits_dir: Path):
    framelist = get_framelist(images_dir)

    scenepts = [1] + scene_cuts + [framelist[-1][0] + 1]

    scenedirs = []

    for n, (startpts, endpts) in enumerate(zip(scenepts, scenepts[1:])):
        scenedir = splits_dir / ('shot' + f'{n:03}' + '_' + str(startpts))
        os.makedirs(scenedir, exist_ok=True)
        scene_image_paths = [images_dir / f[1] for f in framelist if f[0] >= startpts and f[0] < endpts]
        for f in scene_image_paths:
            shutil.copy(str(f), scenedir / f.name)
        scenedirs.append(scenedir)
    return scenedirs


def predict_dir(images_dir: Path, method: KeyPointMethod, model) -> list:
    imgs_paths = sorted(list(images_dir.absolute().glob('*.jpg')))
    if len(imgs_paths) < 13:
        print('Not enough ({}) frames to run prediction, marking all as untrackable.'.format(len(imgs_paths)))
        return [0.0 for _ in range(len(imgs_paths))]
    start_time = time.time()
    features_named = generate_features(imgs_paths, method)
    total_time = time.time() - start_time
    fps = len(imgs_paths) / total_time
    print("Features detected in {} seconds, fps: {}".format(total_time, fps))

    assert (len(features_named) > 0)
    features = [feature for name, feature in features_named.items() if name not in ['t']]
    it = iter(features)
    the_len = len(next(it))
    assert (all(len(l) == the_len for l in it))
    features = np.array([feature_seq for feature_seq in zip(*(features))])

    pred = list(zip(*model.predict_proba(features)))[1]
    return pred


def run(video_id: str, model_id: str, output_dir: str, no_upload: bool):

    temp_dir = Path('/tmp/slam-pred-tmp/')
    shutil.rmtree(temp_dir, ignore_errors=True)

    avpp_agent.init(imagetype='slam-quality-prediction', temp_directory=temp_dir)

    files, _ = avpp_agent.download_media(video_id,
                                         output_dir,
                                         mtypes_required=[MediaType.VideoFullFrames, MediaType.SceneChanges],
                                         skip_downloaded=True)
    model, model_meta = avpp_agent.get_checkpoint(model_id, PickleCheckpoint)

    images_dir = files[MediaType.VideoFullFrames]
    with open(files[MediaType.SceneChanges]) as json_file:
        scene_cuts = json.load(json_file)

    avpp_agent.task_start(MediaType.SLAMPrediction)

    scenes_root = Path(output_dir) / 'scenes'
    os.makedirs(scenes_root)

    scenes_dirs = split_scenes(scene_cuts, images_dir, scenes_root)

    pred = []
    for scene_dir in scenes_dirs:
        pred_scene = predict_dir(scene_dir, KeyPointMethod[model_meta['method']], model)
        pred.append(pred_scene)

    out_file = Path(output_dir) / 'slam_prediction.json'
    with open(out_file, 'w') as outfile:
        json.dump(pred, outfile, indent=2)

    avpp_agent.task_end()
    avpp_agent._verbose = True
    avpp_agent._noupload = no_upload
    avpp_agent.upload_media(video_id, {MediaType.SLAMPrediction: out_file})

    print('All operations completed!')


def parse_run(args: dict):
    video_id = args['--video_id']
    model_id = args['--model_id']
    output_dir = args['--output_dir']
    no_upload = args['--no_upload']
    run(video_id, model_id, output_dir, no_upload)


if __name__ == "__main__":
    args = docopt(__doc__)
    parse_run(args)
