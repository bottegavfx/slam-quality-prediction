from .base import KeypointDetectorMatcher

from enum import Enum

import numpy as np
import cv2 as cv


class CVMethod(Enum):
    SIFT = 1
    ORB = 2


class OpenCVKP(KeypointDetectorMatcher):
    def __init__(self, method: CVMethod):
        if method == CVMethod.SIFT:
            self.kpdetector = cv.SIFT_create(nfeatures=1000, sigma=2.0)
            self.matcher = cv.BFMatcher(crossCheck=True)
        elif method == CVMethod.ORB:
            self.kpdetector = cv.ORB_create(nfeatures=1000)
            self.matcher = cv.BFMatcher(normType=cv.NORM_HAMMING, crossCheck=True)
        else:
            raise RuntimeError('Invalid detector type!')

    def getKeypoints(self, img: np.array) -> (np.array, np.array):
        kp, desc = self.kpdetector.detectAndCompute(img, None)
        return [k.pt for k in kp], desc

    def matchKeypoints(self, kp_desc1, kp_desc2):
        kp1, des1 = kp_desc1
        kp2, des2 = kp_desc2

        if des1 is None or des2 is None or len(des1) == 0 or len(des2) == 0:
            return [], []

        matches = self.matcher.match(des1, des2)

        matches = sorted(matches, key=lambda x: x.distance)
        pts1 = [kp1[x.queryIdx] for x in matches]
        pts2 = [kp2[x.trainIdx] for x in matches]
        return pts1, pts2