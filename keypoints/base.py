import numpy as np

class KeypointDetectorMatcher():
    kpdetector = None
    matcher = None

    def getKeypoints(self, img: np.array):
        pass

    def matchKeypoints(self, kp_desc1, kp_desc2) -> (np.array, np.array):
        pass

    def close(self):
        if self.matcher is not None: del self.matcher
        if self.kpdetector is not None: del self.kpdetector