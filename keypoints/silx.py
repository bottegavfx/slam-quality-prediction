from .base import KeypointDetectorMatcher

import numpy as np
import silx.image.sift as sift


# def silxKeypointsToCv(kp_desc):
#     kp = []
#     desc = []
#     for k_d in kp_desc:
#         k = cv.KeyPoint()
#         k.pt = (k_d.x, k_d.y)
#         kp.append(k)
#         desc.append(k_d.desc)
#     return kp, desc


class SilxKP(KeypointDetectorMatcher):
    @staticmethod
    def _extractXYviewFromSilx(kp_desc):
        r = kp_desc.astype([('x', '<i4'), ('y', '<i4'), ('scale', '<i4'), ('angle', '<i4'),
                            ('desc', 'i4', (128, ))]).view('<i4').reshape(len(kp_desc), 132)
        g = np.take(r, [0, 1], axis=1)
        return g

    def __init__(self, shape: tuple, dtype: np.dtype):
        self.kpdetector = sift.SiftPlan(shape, dtype)
        self.matcher = sift.MatchPlan()

    def getKeypoints(self, img: np.array) -> np.recarray:
        kp_desc = self.kpdetector.keypoints(img)
        return kp_desc

    def matchKeypoints(self, kp_desc1: np.recarray, kp_desc2: np.recarray) -> (np.ndarray, np.ndarray):
        if len(kp_desc1) == 0 or len(kp_desc2) == 0: return [], []
        matches = self.matcher.match(kp_desc1, kp_desc2)
        if len(matches) > 0:
            m1, m2 = matches.transpose()
            kp1 = self._extractXYviewFromSilx(m1)
            kp2 = self._extractXYviewFromSilx(m2)
        else:
            kp1 = []
            kp2 = []
        return kp1, kp2