import itertools
import multiprocessing
from multiprocessing.pool import ThreadPool
from collections import defaultdict
from enum import Enum
from typing import List, Dict, Tuple
from pathlib import Path

import numpy as np
import cv2 as cv

from keypoints.base import KeypointDetectorMatcher

class KeyPointMethod(Enum):
    OPENCV_SIFT = 1
    OPENCV_ORB = 2
    SILX_SIFT = 3

    def __str__(self):
        return self.name


def _generate_matching_list(img_paths: list, pos: int, overlap: int, start: int = 1) -> list:
    matches_list = []
    ind1 = pos
    for offset in range(start, overlap + 1):
        ind2 = ind1 + offset
        if ind2 < len(img_paths):
            im2 = img_paths[ind2]
            if ind1 != ind2:
                matches_list.append((ind2, im2))
    return matches_list


def _get_normalized_resolution(orig_h: int, orig_w: int) -> Tuple[int, int]:
    if orig_w > orig_h:
        new_w, new_h = int((540.0 / orig_h) * orig_w), 540
    else:
        new_w, new_h = 540, int((540.0 / orig_w) * orig_h)
    return new_w, new_h


def _calculate_point_visibility_score(points: np.array, im_width: int, im_height: int) -> float:
    num_levels = 4
    max_score = 0
    pyramid = []

    for level in range(num_levels):
        dim = 1 << (level + 1)
        pyramid.append(np.zeros((dim, dim), dtype=np.int32))
        max_score += dim * dim * dim * dim

    max_dim = 1 << len(pyramid)
    points_rescaled = np.int32((points * max_dim) / [im_width, im_height])
    np.clip(points_rescaled, 0, max_dim - 1)

    score = 0
    for level in reversed(pyramid):
        increments = np.bincount(np.ravel_multi_index(points_rescaled.T, level.shape))
        increments = np.hstack([increments, np.zeros(level.size - increments.size, dtype=np.int32)])
        increments = np.resize(increments, level.shape).T
        level += increments
        score += np.count_nonzero(level >= 1) * level.size
        points_rescaled = np.right_shift(points_rescaled, 1)

    return score / max_score


def _summarize_metrics_series(series: defaultdict(list), suffix: str = '') -> dict:
    summarized_metrics = {}
    for metric_name, metric_series in series.items():
        metric_name_s = metric_name if suffix == '' else metric_name + '_' + suffix
        if len(metric_series) > 0:
            summarized_metrics[metric_name_s] = {
                'mean': float(np.mean(metric_series)),
                'median': float(np.median(metric_series)),
                'min': float(np.min(metric_series)),
                'max': float(np.max(metric_series)),
                'p10': float(np.percentile(metric_series, 10)),
                'p90': float(np.percentile(metric_series, 90)),
            }
        else:
            summarized_metrics[metric_name_s] = {
                'mean': float(0.0),
                'median': float(0.0),
                'min': float(0.0),
                'max': float(0.0),
                'p10': float(0.0),
                'p90': float(0.0),
            }
    return summarized_metrics


def _flatten_imgs_metrics(metrics: list) -> dict:
    flattened_metrics = defaultdict(list)
    for image in metrics:
        for metric_basename, metric_base in image.items():
            if isinstance(metric_base, dict):
                for metric_suffix, value in metric_base.items():
                    flattened_metrics[metric_basename + '_' + metric_suffix].append(value)
            else:
                flattened_metrics[metric_basename].append(metric_base)
    it = iter(flattened_metrics.items())
    the_len = len(next(it)[1])
    assert(all(len(l[1]) == the_len for l in it))
    return dict(flattened_metrics)


def _calculate_metrics_kernel(img_no: int, imgs_paths: List[Path],
                              imgs_matches: Dict[Tuple[int, int], Dict[int, np.array]],
                              num_img_keypoints: int, kp_method: KeyPointMethod, im_width: int, im_height: int):
    image_metrics_series_close = defaultdict(list)
    image_metrics_series_far = defaultdict(list)

    # img_path = imgs_paths[img_no]
    # print('Verifying {}:'.format(img_path.name))
    for p, m in imgs_matches.items():
        if img_no in p:
            target_img_no = p[0] if p[1] == img_no else p[1]
            # target_img_path = imgs_paths[target_img_no]
            pts = m[img_no]
            target_pts = m[target_img_no]
            assert(len(pts) == len(target_pts))
            num_matches = len(pts)

            dt = img_no - target_img_no

            if kp_method == KeyPointMethod.SILX_SIFT:
                pts_coords = pts
                target_pts_coords = target_pts
            else:
                pts_coords = np.int32(pts)
                target_pts_coords = np.int32(target_pts)

            if len(pts_coords) > 8 and len(target_pts_coords) > 8:
                _, mask_F = cv.findFundamentalMat(pts_coords, target_pts_coords, cv.FM_RANSAC, 2, 0.99)
                _, mask_H = cv.findHomography(pts_coords, target_pts_coords, method=cv.RANSAC, ransacReprojThreshold=2, confidence=0.99)
            else:
                mask_F = None
                mask_H = None

            if mask_F is not None and len(mask_F) > 0 and np.max(mask_F) > 0:
                F_inliers = [pts[i] for i, c in enumerate(mask_F.ravel().astype(bool)) if c] # ...
                F_inliers_coords = pts_coords[mask_F.ravel().astype(bool)]
                F_num_inliers = len(F_inliers)
                F_inlier_ratio = F_num_inliers / num_matches

                F_inliers_coords_target = target_pts_coords[mask_F.ravel().astype(bool)]

                pts_deltas = np.sqrt(((F_inliers_coords - F_inliers_coords_target)**2).sum(axis=1))
                pts_distance = abs(np.median(pts_deltas)) / (abs(dt))
                pts_dev = np.std(pts_deltas)

                dist_score = _calculate_point_visibility_score(F_inliers_coords, im_width, im_height) / F_num_inliers
            else:
                F_num_inliers = 0
                F_inlier_ratio = 0.0
                dist_score = 0.0
                pts_distance = 0.0
                pts_dev = 10000.0

            if mask_H is not None and len(mask_H) > 0 and np.max(mask_H) > 0:
                H_num_inliers = np.count_nonzero(mask_H.ravel())
                H_inlier_ratio = H_num_inliers / num_matches
            else:
                H_num_inliers = 0
                H_inlier_ratio = 0.0

            H_F_inlier_ratio = H_num_inliers / F_num_inliers if H_num_inliers > 0 and F_num_inliers > 0 else 1.0

            if abs(dt) > 5:
                image_metrics_series = image_metrics_series_far
            else:
                image_metrics_series = image_metrics_series_close
            image_metrics_series['num_matches'].append(num_matches)
            image_metrics_series['F_num_inliers'].append(F_num_inliers)
            image_metrics_series['F_inlier_ratio'].append(F_inlier_ratio)
            image_metrics_series['H_num_inliers'].append(H_num_inliers)
            image_metrics_series['H_inlier_ratio'].append(H_inlier_ratio)
            image_metrics_series['H_F_inlier_ratio'].append(H_F_inlier_ratio)
            image_metrics_series['dist_score'].append(dist_score)
            image_metrics_series['pts_distance'].append(pts_distance)
            image_metrics_series['pts_dev'].append(pts_dev)

    img_metrics = { **_summarize_metrics_series(image_metrics_series_close, 'c'),
                    **_summarize_metrics_series(image_metrics_series_far, 'f') }
    img_metrics['keypoints'] = num_img_keypoints
    return img_metrics


def _detect_keypoints_kernel(img_path: Path):
    global kpdetector
    img = cv.imread(str(img_path))
    gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
    gray = cv.resize(gray, _get_normalized_resolution(*gray.shape), interpolation=cv.INTER_AREA)#.astype('float32')
    assert(kpdetector is not None)
    kp_desc = kpdetector.getKeypoints(gray)
    num_keypoints = len(kp_desc[0]) if isinstance(kp_desc, tuple) else len(kp_desc)
    print('Detected {} keypoints for {}'.format(num_keypoints, img_path.name))
    return kp_desc
    # img = cv.drawKeypoints(gray, kp, img)
    # img_keypoints_vis_path = imgs_keypoints_vis_path / img_path.name
    # cv.imwrite(str(img_keypoints_vis_path), img)


def _match_keypoints_kernel(img_no: int, imgs_paths: List[Path],
                            imgs_keypoints) -> Dict[Tuple[int, int], Dict[int, np.array]]:
    global kpdetector
    imgs_matches = {}
    img_path = imgs_paths[img_no]
    print('Matching {}:'.format(img_path.name))
    target_imgs = _generate_matching_list(imgs_paths, img_no, 8, 1)
    for target_img_no, _ in target_imgs:
        pts, target_pts = kpdetector.matchKeypoints(imgs_keypoints[img_no], imgs_keypoints[target_img_no])
        pair = tuple(sorted([img_no, target_img_no]))
        assert(pair not in imgs_matches)
        imgs_matches[pair] = { img_no: np.array(pts), target_img_no: np.array(target_pts) }
    return imgs_matches

kpdetector = None

def generate_features(imgs_paths: List[Path], kp_method: KeyPointMethod) -> dict:
    if len(imgs_paths) < 13:
        raise ValueError('Not enough images provided, must have at least 13 images in the list!')

    im_width = 0
    im_height = 0
    img = cv.imread(str(imgs_paths[0]))
    gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
    gray = cv.resize(gray, _get_normalized_resolution(*gray.shape), interpolation=cv.INTER_AREA)
    im_height, im_width = gray.shape

    global kpdetector
    kpdetector = None

    if kp_method == KeyPointMethod.SILX_SIFT:
        from keypoints.silx import SilxKP
        kpdetector = SilxKP(gray.shape, gray.dtype)
    elif kp_method == KeyPointMethod.OPENCV_SIFT:
        from keypoints.opencv import OpenCVKP, CVMethod
        kpdetector = OpenCVKP(CVMethod.SIFT)
    elif kp_method == KeyPointMethod.OPENCV_ORB:
        from keypoints.opencv import OpenCVKP, CVMethod
        kpdetector = OpenCVKP(CVMethod.ORB)

    pool = ThreadPool()
    # pool = multiprocessing.Pool()

    if kp_method == KeyPointMethod.SILX_SIFT:
        imgs_keypoints = list(map(_detect_keypoints_kernel, imgs_paths))
    else:
        imgs_keypoints = pool.starmap(_detect_keypoints_kernel, zip(imgs_paths))

    assert(len(imgs_keypoints) > 8)

    if kp_method == KeyPointMethod.SILX_SIFT:
        imgs_matches = {}
        list(map(_match_keypoints_kernel, range(len(imgs_paths)), itertools.repeat(imgs_paths), itertools.repeat(imgs_matches),
                itertools.repeat(imgs_keypoints)))
    else:
        imgs_matches_list = list(map(_match_keypoints_kernel, range(len(imgs_paths)), itertools.repeat(imgs_paths), itertools.repeat(imgs_keypoints)))
        imgs_matches = {}
        for match_list in imgs_matches_list:
            imgs_matches = {**imgs_matches, **match_list}

    imgs_metrics = pool.starmap(
        _calculate_metrics_kernel,
        zip(range(len(imgs_paths)), itertools.repeat(imgs_paths), itertools.repeat(imgs_matches), [len(x) for x in imgs_keypoints],
            itertools.repeat(kp_method), itertools.repeat(im_width), itertools.repeat(im_height)))

    # imgs_metrics = list(map(_calculate_metrics_kernel,
    #                     range(len(imgs_paths)),
    #                     itertools.repeat(imgs_paths),
    #                     itertools.repeat(imgs_matches),
    #                     [len(x) for x in imgs_keypoints],
    #                     itertools.repeat(kp_method),
    #                     itertools.repeat(im_width),
    #                     itertools.repeat(im_height)))

    pool.close()
    kpdetector.close()

    imgs_metrics_flattened = _flatten_imgs_metrics(imgs_metrics)

    return imgs_metrics_flattened