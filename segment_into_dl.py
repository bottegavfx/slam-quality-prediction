"""
Usage:
    segment_into_dl.py --video_id <video_id> [options]

Options:
    --video_id <video_id>   Video ID (from video-index)
    --output_dir <path>     Output directory [default: /tmp/datalake-slam-segment/]
    --place_with_tag <tag>  Upload all segments to segment-index and add this slug
    --min_seg_len <sec>     Minimal segment length [default: 3.0]
    --max_failed <nframes>  Maximum number of sequentially failed frames [default: 3]
    --no_upload             Skip uploading anything to Datalake
"""


import os
import re
import json
import shutil
from pathlib import Path
from typing import List, Tuple

from docopt import docopt

import avpp_kit.agent as avpp_agent
from avpp_kit.agent import MediaType
from avpp_kit.processing.meta.rgb import get_metadata

from datalake.datalake import Datalake

def find_segments(timeseries: List[List[float]],
                  fps: float,
                  min_length_s: float = 2.0,
                  th_valid_p: float = 0.5,
                  th_tolerance_f: int = 3) -> List[Tuple[int]]:
    assert(len(timeseries) > 0)
    if isinstance(timeseries[0], float): timeseries = [timeseries]

    segments = []
    frame_n = 1
    for p_t in timeseries:
        valid_for = 0
        invalid_for = 0
        segment_start = frame_n
        for p in p_t:
            if p > th_valid_p:
                if valid_for == 0: segment_start = frame_n
                valid_for += 1
                invalid_for = 0
            else:
                invalid_for += 1
                if invalid_for > th_tolerance_f:
                    if valid_for > min_length_s * fps:
                        segments.append((segment_start, frame_n - 1 - invalid_for))
                    valid_for = 0
            frame_n += 1
        if valid_for > min_length_s * fps:
            segments.append((segment_start, frame_n - 1))
    return segments


def run(video_id: str, placement_tag: str, output_dir: str, no_upload: bool, min_seg_len: float, max_failed: int):

    temp_dir = Path('/tmp/slam-seg-tmp/')
    shutil.rmtree(temp_dir, ignore_errors=True)

    avpp_agent.init(imagetype='slam-segment-generation', temp_directory=temp_dir)

    files, files_meta = avpp_agent.download_media(video_id,
                                                  output_dir,
                                                  mtypes_required=[MediaType.SLAMPrediction, MediaType.VideoFullFrames],
                                                  skip_downloaded=True)

    images_dir = files[MediaType.VideoFullFrames]

    avpp_agent.task_start(MediaType.VideoSegments)

    with open(files[MediaType.SLAMPrediction]) as json_file:
        timeseries = json.load(json_file)

    segments = find_segments(timeseries, files_meta['video']['fps'], min_length_s=min_seg_len, th_tolerance_f=max_failed)
    segments_path = Path(output_dir) / 'segments.json'

    with open(segments_path, 'w') as outfile:
        s = json.dumps(segments, indent=2)
        s = re.sub(r'\s{2}\[\s*(\d+),\s*(\d+)\s*\](,?)', r'  [\1, \2]\3', s)
        outfile.write(s)

    avpp_agent.task_end()

    avpp_agent._verbose = True
    avpp_agent._noupload = no_upload
    avpp_agent.upload_media(video_id, { MediaType.VideoSegments: segments_path })

    if placement_tag is not None:
        segments_root_dir = Path(output_dir) / 'segments'
        shutil.rmtree(segments_root_dir, ignore_errors=True)
        os.makedirs(segments_root_dir)

        Datalake.CONFIG = None
        dl = Datalake()

        slugs = files_meta['video']['slugs']
        slugs.append(placement_tag)

        for start_frame, end_frame in segments:
            segment_dir = segments_root_dir / ('segment_{}'.format(str(start_frame)))
            os.makedirs(segment_dir)
            segment_frames = list(range(start_frame, end_frame + 1, 1))
            for f in segment_frames:
                shutil.copy(images_dir / 'rgb.{}.jpg'.format(str(f).zfill(7)), segment_dir)

            asset_name = files_meta['video']['name'].lower().replace(' ', '_') + '_' + segment_dir.stem
            print('Creating asset `{}` with {} frames'.format(asset_name, len(segment_frames)))
            if no_upload: continue
            asset_id = dl.create_asset(asset_name=asset_name,
                                    fps=files_meta['video']['fps'],
                                    frames_amount=len(segment_frames),
                                    slugs=slugs,
                                    user_data={
                                        'original_url': 'none',
                                        'original_desc': 'none',
                                        'original_tags': 'none',
                                        'camera_model': 'unknown',
                                        'video_id': files_meta['video']['id'],
                                        'segments_id': avpp_agent.pushedartifactids[Datalake.VideoType.Segments]
                                    })
            artifact_id = dl.add_artifact(asset_id=asset_id,
                                        artifact_name='rgb_orig_jpg',
                                        folder_path=str(segment_dir),
                                        data_type=Datalake.DataType.ImagesTarRGB,
                                        metadata=get_metadata(segment_dir))
    print('All operations completed!')

def parse_run(args: dict):
    video_id = args['--video_id']
    placement_tag = args['--place_with_tag']
    output_dir = args['--output_dir']
    no_upload = bool(args['--no_upload'])
    min_seg_len = float(args['--min_seg_len'])
    max_failed = int(args['--max_failed'])
    run(video_id, placement_tag, output_dir, no_upload, min_seg_len, max_failed)

if __name__ == "__main__":
    args = docopt(__doc__)
    parse_run(args)