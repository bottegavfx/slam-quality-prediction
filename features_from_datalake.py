import json
import time
import argparse
from pathlib import Path

from natsort import natsorted
from datalake.datalake import Datalake

from generate_features import generate_features, KeyPointMethod

parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('asset_id', help='Asset ID', type=str)
parser.add_argument('-o', '--output', help='Output directory', type=str, default='.')
parser.add_argument('--kp_method', type=lambda k: KeyPointMethod[k], choices=list(KeyPointMethod), default=KeyPointMethod.SILX_SIFT)
args = vars(parser.parse_args())

asset_id = args['asset_id']
out_dir = Path(args['output'])
assert(out_dir.exists())

Datalake.CONFIG = None
dl = Datalake()
asset_meta = dl.get_asset(id=asset_id)
fps = asset_meta['fps']
imgs_src_path = dl.fetch_download(return_current=True, asset_id=asset_id, destination='/tmp/', data_type=Datalake.DataType.ImagesTarRGB)
if isinstance(imgs_src_path, list):
    imgs_src_path = Path(imgs_src_path[0])
else:
    imgs_src_path = Path(imgs_src_path)
imgs_paths = natsorted(list(imgs_src_path.absolute().glob('*.jpg')))

start_time = time.time()

features = generate_features(imgs_paths, args['kp_method'])

total_time = time.time() - start_time
fps = len(imgs_paths) / total_time
print("Executed in {} seconds, fps: {}".format(total_time, fps))

with open(out_dir / (asset_id + '_metrics.json'), 'w') as outfile:
    json.dump(features, outfile, indent=4)