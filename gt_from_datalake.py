import os
import json
import argparse

import cv2 as cv

import avpp_kit.agent as avpp_agent
from avpp_kit.agent import MediaType
from avpp_kit.processing.cameradata.colmapproc import ColmapCameraDataProc

def GetNormalizedResolution(orig_h, orig_w):
    if orig_w > orig_h:
        new_w, new_h = int((540.0 / orig_h) * orig_w), 540
    else:
        new_w, new_h = 540, int((540.0 / orig_w) * orig_h)
    return new_w, new_h

parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('asset_id', help='Asset ID', type=str)
parser.add_argument('-o', '--output', help='Output directory', type=str, default='/tmp/datalake-qa/')
parser.add_argument('--no-strict', action='store_true')
parser.add_argument('--no-rescale', action='store_true')
args = vars(parser.parse_args())

avpp_agent.init(temp_directory=args['output'] + '/' + args['asset_id'])

return_current = True

if args['no_strict']:
    return_current = False
    for artifact in avpp_agent.mediatypemappings[MediaType.CameraData]['artifacts']:
        artifact['condition'] = lambda x: x['name'].endswith('_no_strict')

files, meta = avpp_agent.download_media(args['asset_id'],
                                        args['output'] + '/' + args['asset_id'],
                                        mtypes_required=[MediaType.RGB, MediaType.CameraData],
                                        purge_dest=True,
                                        # skip_downloaded=True,
                                        return_current=return_current)

# completely failed to track
rgb_idxes, rgb_names = zip(*avpp_agent.mediaframelistgenerators[MediaType.RGB](files[MediaType.RGB]))
cam_idxes, cam_names = zip(*sorted(avpp_agent.mediaframelistgenerators[MediaType.CameraData](files[MediaType.CameraData]), key=lambda x: x[0]))
untracked_idxes = list(set(rgb_idxes).difference(set(cam_idxes)))

# erroneous tracks
cameras, images, points = ColmapCameraDataProc.read_cameradata(files[MediaType.CameraData])
imgs_metrics = ColmapCameraDataProc.get_images_metrics(images)
wndfm = [0.0] + imgs_metrics['wndfm']

invalid_idxes = []

thresh_crit = 0.125
thresh_warn = 0.075
invalid_frames = 0
for i, v in enumerate(wndfm):
    if v < thresh_warn:
        continue
    elif v >= thresh_warn and v < thresh_crit:
        print('Possible camera trajectory inconsistency at frame {} with WNDFM of {}.'.format(i, v))
    elif v >= thresh_crit:
        print('Critical camera trajectory inconsistency at frame {} with WNDFM of {}.'.format(i, v))
        invalid_idxes.append(cam_idxes[i])

os.makedirs(args['output'] + '/' + args['asset_id'] + '/thumbs', exist_ok=True)

trackquality = {}

for rgb_idx, rgb_name in zip(rgb_idxes, rgb_names):
    img = cv.imread(str(files[MediaType.RGB] / rgb_name))
    resized = cv.resize(img, GetNormalizedResolution(*img.shape[0:2]), interpolation=cv.INTER_AREA)
    cv.putText(resized, 'wndfm: {:.4f}'.format(wndfm[cam_idxes.index(rgb_idx)] if rgb_idx in cam_idxes else 1.0), (0, 50), cv.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 0), 2)
    cv.putText(resized, 'real idx: {}'.format(rgb_idx), (0, 75), cv.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 0), 2)
    cv.putText(resized, 'reco idx: {}'.format(cam_idxes.index(rgb_idx) if rgb_idx in cam_idxes else -1), (0, 100), cv.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 0), 2)
    cv.putText(resized, 'tracked: {}'.format(rgb_idx not in untracked_idxes), (0, 125), cv.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 0), 2)
    reconstructed = rgb_idx not in untracked_idxes and rgb_idx in cam_idxes and rgb_idx not in invalid_idxes
    cv.putText(resized, 'reconstructed: {}'.format(reconstructed), (0, 150), cv.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 0) if reconstructed else (0, 0, 255), 2)
    cv.imwrite(args['output'] + '/' + args['asset_id'] + '/thumbs/' + rgb_name, resized)
    trackquality[rgb_name] = reconstructed

with open(args['output'] + '/' + args['asset_id'] + '/track.json', 'w') as outfile:
    json.dump(trackquality, outfile, indent=4)